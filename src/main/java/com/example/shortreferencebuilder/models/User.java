package com.example.shortreferencebuilder.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Date: 22.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * The {@code User} entity. Used for create a user's account and to control his rights.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "account")
public class    User {

    /**
     * Available user's roles.
     */
    public enum Role {
        USER, ADMIN;
    }

    /**
     * Available user's states.
     */
    public enum State {
        NOT_CONFIRMED, CONFIRMED, BANNED, DELETED;
    }

    /**
     * User's ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Date and time of user's creation.
     */
    @Column(columnDefinition = "TIMESTAMP NOT NULL")
    private LocalDateTime creationDateTime;

    /**
     * User's email.
     */
    @Column(columnDefinition = "VARCHAR (30) NOT NULL")
    private String email;

    /**
     * User's hashed password.
     */
    @Column(columnDefinition = "VARCHAR (100) NOT NULL")
    private String hashPassword;

    /**
     * User's first name.
     */
    @Column(columnDefinition = "VARCHAR (20)")
    private String firstName;

    /**
     * User's last name.
     */
    @Column(columnDefinition = "VARCHAR (20)")
    private String lastName;

    /**
     * User's state.
     */
    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private State state;

    /**
     * User's role.
     */
    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private Role role;

    /**
     * List of the {@link Reference references}, which was created and paid by the user.
     */
    @OneToMany(mappedBy = "ownerUser")
    @ToString.Exclude
    private List<Reference> createdReferences;
}
