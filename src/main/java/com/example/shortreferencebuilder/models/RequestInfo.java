package com.example.shortreferencebuilder.models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * The {@code Request} entity.
 * Provides the data about requests to redirect URL.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "request")
public class RequestInfo {

    /**
     * {@code Request's} ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * {@code Request's} date and time of creation.
     */
    @Column(columnDefinition = "TIMESTAMP NOT NULL")
    private LocalDateTime creationDateTime;

    /**
     * {@code Reference} ID, which {@code request} is belongs to.
     */
    @ManyToOne
    @JoinColumn(name = "reference_id", columnDefinition = "BIGINT NOT NULL")
    private Reference reference;

    /**
     * A Requester's IP address.
     */
    private String address;

    /**
     * A Requester's host name.
     */
    private String host;

    /**
     * A Requester's host port.
     */
    private Integer port;

    /**
     * A {@code User-Agent} header value.
     */
    private String userAgent;

    /**
     * Result of redirection.
     */
    private Boolean redirected;

    /**
     * Geographic country, which {@code request's} IP belongs to.
     */
    private String country;

    /**
     * Geographic city, which {@code request's} IP belongs to.
     */
    private String city;

    /**
     * Geographic longitude, which {@code request's} IP belongs to.
     */
    private Double longitude;

    /**
     * Geographic latitude, which {@code request's} IP belongs to.
     */
    private Double latitude;

    /**
     * Temperature in degrees at the moment of {@code request}.
     */
    private Integer temperature;

    /**
     * Weather condition at the moment of {@code request}.
     */
    private String weatherCondition;

    /**
     * List of {@link CookieInfo Cookies}
     */
    @OneToMany(mappedBy = "requestInfo", cascade = CascadeType.REMOVE)
    @ToString.Exclude
    private List<CookieInfo> cookies;
}
