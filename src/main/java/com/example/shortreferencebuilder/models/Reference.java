package com.example.shortreferencebuilder.models;

import com.example.shortreferencebuilder.config.EnvironmentUtil;
import lombok.*;

import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Date: 22.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * The {@code Reference} entity.
 * Combines main parameters and methods of a short link ({@code reference}).
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Reference {

    /**
     * Maximum length of the {@code reference} name.
     */
    public static final Integer SHORTREFERENCE_MAXSIZE = 8  ;

    /**
     * Default value for {@code freePeriod} field.
     * Is used to count expiration date when new {@code reference} has been creating.
     */
    public static final Integer DEFAULT_FREE_PERIOD_DAYS = 5;

    /**
     * Permitted symbols for short {@code reference's} name.
     */
    public static final String SHORTREFERENCE_ALLOWED_CHARS =
            "1234567890" +
                    "ABCDEFGHIJKLMNQRSTUVWXYZ" +
                    "abcdefghijklmnopqrstuvwxyz" +
                    "-_";

    /**
     * Available {@code reference's} state.
     */
    public enum State {
        ACTIVE, NOT_ACTIVE;
    }

    /**
     * {@code Reference's} ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Date and time of {@code reference} creation.
     */
    @Column(columnDefinition = "TIMESTAMP NOT NULL")
    private LocalDateTime creationDateTime;

    /**
     * Date ant time of {@code reference} expiration.
     */
    @Column(columnDefinition = "TIMESTAMP NOT NULL")
    private LocalDateTime expirationDateTime;


    /**
     * {@code Reference's} short name.
     */
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private String shortRef;

    /**
     * {@code Reference's} redirect URL.
     */
    @Column(columnDefinition = "VARCHAR NOT NULL")
    private String redirectRef;

    /**
     * {@code Reference's} state.
     */
    @Enumerated(value = EnumType.STRING)
    @Column(columnDefinition = "VARCHAR (20) NOT NULL")
    private State state;

        /**
         * {@link User}, who owns the {@code reference}.
         */
    @ManyToOne
    @JoinColumn(name = "owner_user_id", columnDefinition = "BIGINT")
    private User ownerUser;

    /**
     * List of {@link RequestInfo requests} for redirection to the redirect URL.
     */
    @OneToMany(mappedBy = "reference", cascade = CascadeType.REMOVE)
    @ToString.Exclude
    private List<RequestInfo> requests;

    /**
     * Invokes {@code test()} method of Predicate functional interface.
     */
    static public <T> boolean validate(Predicate<T> predicate, T t) {
        return predicate.test(t);
    }

    /**
     * Invokes {@code test()} method of BiPredicate functional interface.
     */
    static public <T, U> boolean validate(BiPredicate<T, U> biPredicate, T t, U u) {
        return biPredicate.test(t, u);
    }

    /**
     * Invokes {@code get()} method of Supplier functional interface.
     */
    static public String create(Supplier<String> supplier) {
        return supplier.get();
    }

    /**
     * Combines {@code reference's} validation methods.
     */
    static public class ReferenceValidator {

        /**
         * Checks the {@code short name} for allowed symbols.
         *
         * @param shortReference short name
         * @return true if all symbols are matching pattern.
         */
        static public boolean checkChars(String shortReference) {
            Boolean result = false;

            if (shortReference != null
                    && shortReference.length() <= SHORTREFERENCE_MAXSIZE
                    && shortReference.length() > 0) {
                for (int i = 0; i < shortReference.length(); i++) {
                    if (SHORTREFERENCE_ALLOWED_CHARS.indexOf(shortReference.charAt(i)) < 0) {
                        return false;
                    }
                    result = true;
                }
            }
            return result;
        }

        /**
         * Checks {@code reference} expiration date.
         *
         * @param reference {@link Reference} object.
         * @return true if expiration date has passed.
         */
        static public boolean isExpired(Reference reference) {
            return LocalDateTime.now().isAfter(reference.getExpirationDateTime());
        }

        /**
         * Checks the access rights to the {@code reference}.
         *
         * @param reference {@link Reference} object.
         * @param user      {@link User} object.
         * @return true if user is owner or has ADMIN role.
         */
        static public boolean isUserHasAuthoritiesToGetReference(Reference reference, User user) {
            return reference.getOwnerUser() != null
                    && reference.getOwnerUser().equals(user)
                    || user.getRole().equals(User.Role.ADMIN);
        }

        /**
         * Checks if finally URL is correct.
         *
         * @param redirectUrl URL as string.
         * @param request     request for redirection, supported by Spring.
         * @return true if URL correct
         */
        static public boolean isUrlCorrect(String redirectUrl, HttpServletRequest request) {
            try {
                // try to map URL to java.net.URL.class
                URL url = new URL(redirectUrl);

                String redirectHost = url.getHost();
                String localHost = new EnvironmentUtil().getHostName();

                // compare URL's host name and local host name
                // shouldn't be matched
                return !redirectHost.equals(localHost);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    /**
     * Short link's name creator.
     */
    static public class ReferenceCreator {

        /**
         * Randomly generates the short link's name.
         *
         * @return short link's name as string.
         */
        public static String createShortReference() {
            StringBuilder str = new StringBuilder();
            char[] chars = SHORTREFERENCE_ALLOWED_CHARS.toCharArray();
            Random rand = new Random();
            for (int i = 0; i < SHORTREFERENCE_MAXSIZE; i++) {
                shuffleArray(chars);
                str.append(chars[rand.nextInt(chars.length)]);
            }
            return str.toString();
        }

        /**
         * Shuffles the symbols' array after generation of each symbol.
         *
         * @param array symbols
         */
        static void shuffleArray(char[] array) {
            Random rnd = ThreadLocalRandom.current();
            int index;
            char temp;
            for (int i = array.length - 1; i > 0; i--) {
                index = rnd.nextInt(i + 1);
                temp = array[index];
                array[index] = array[i];
                array[i] = temp;
            }
        }
    }

}
