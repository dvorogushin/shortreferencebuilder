package com.example.shortreferencebuilder.forms;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Date: 23.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Is used to check, identify and transfer data from client to controller during {@code reference} creation.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Setter
@Getter
@Builder
public class ReferenceForm {

    /**
     * Redirect URL.
     */
    @NotNull
    @Length(min = 2, max = 2048)
    private String url;

    /**
     * Free period in days.
     */
    @NotNull
    @Min(1)
    @Max(30)
    private Integer freePeriod;

}
