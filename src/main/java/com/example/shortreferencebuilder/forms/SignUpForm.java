package com.example.shortreferencebuilder.forms;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Is used to check, identify and transfer data from client to controller during {@code user} creation.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Getter
@Setter
@Builder
public class SignUpForm {

    /**
     * User's first name.
     */
    @NotNull
    @Length(min = 2, max = 20)
    private String firstName;

    /**
     * User's last name.
     */
    @Length(min = 2, max = 20)
    private String lastName;

    /**
     * User's email.
     */
    @NotNull
    @Length(min = 5, max = 30)
    private String email;

    /**
     * User's password.
     */
    @NotNull
    private String password;
}
