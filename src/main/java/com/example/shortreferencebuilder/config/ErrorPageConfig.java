package com.example.shortreferencebuilder.config;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.ErrorPageRegistrar;
import org.springframework.boot.web.server.ErrorPageRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Обработчик ошибок
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Configuration
public class ErrorPageConfig implements ErrorPageRegistrar {

 @Override
 public void registerErrorPages(ErrorPageRegistry registry) {

  // если статус NOT_FOUND, то в контроллер на "/error/404"
  ErrorPage page400 = new ErrorPage(HttpStatus.BAD_REQUEST, "/error/400");
  ErrorPage page403 = new ErrorPage(HttpStatus.FORBIDDEN, "/error/403");
  ErrorPage page404 = new ErrorPage(HttpStatus.NOT_FOUND, "/error/404");
  ErrorPage page405 = new ErrorPage(HttpStatus.METHOD_NOT_ALLOWED, "/error/405");
  ErrorPage page500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error/500");
  // регистрация страницы
  registry.addErrorPages(page400);
  registry.addErrorPages(page403);
  registry.addErrorPages(page404);
  registry.addErrorPages(page405);
  registry.addErrorPages(page500);
 }
}
