package com.example.shortreferencebuilder.repositories;

import com.example.shortreferencebuilder.transfer.geolocationDto.GeoLocationDto;

/**
 * Date: 03.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Reads local database GeoLite2 to get geographic coordinates.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */

public interface GeoLocationRepository {

    GeoLocationDto findGeoByIP(String URL);
}
