package com.example.shortreferencebuilder.repositories;

import com.example.shortreferencebuilder.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Date: 22.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * User's JPA repository.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAllByEmail(String email);
}
