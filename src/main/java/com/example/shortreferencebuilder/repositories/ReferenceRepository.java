package com.example.shortreferencebuilder.repositories;

import com.example.shortreferencebuilder.models.Reference;
import com.example.shortreferencebuilder.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Date: 23.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Reference's JPA repository.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public interface ReferenceRepository extends JpaRepository<Reference, Long> {

    Optional<Reference> findByShortRefOrderByIdAsc(String shortReference);

    List<Reference> findAllByOwnerUserOrderById(User user);
}
