package com.example.shortreferencebuilder.repositories.impl;

import com.example.shortreferencebuilder.repositories.GeoLocationRepository;
import com.example.shortreferencebuilder.transfer.geolocationDto.GeoLocationDto;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Date: 03.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Reads local database GeoLite2 to get geographic coordinates.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Component
@RequiredArgsConstructor
public class GeoLocationRepositoryImpl implements GeoLocationRepository {

    /**
     * Database GeoLite2 location.
     */
    @Value("${geolocation.source.database}")
    private String databasePath;

    /**
     * Defines geographic coordinates by IP.
     *
     * @param ip as string
     * @return {@link GeoLocationDto} object, includes geographic coordinates etc.
     */
    @Override
    public GeoLocationDto findGeoByIP(String ip) {

        /**
         * Read coordinates from database.
         */
        File database = new File(databasePath);
        try {
            InetAddress inetAddress = InetAddress.getByName(ip);
            try {
                DatabaseReader reader = new DatabaseReader.Builder(database).build();
                CityResponse response = reader.city(inetAddress);

                return GeoLocationDto.builder()
                        .country(response.getCountry().getName())
                        .city(response.getCity().getName())
                        .latitude(response.getLocation().getLatitude())
                        .longitude(response.getLocation().getLongitude())
                        .build();

            } catch (IOException | GeoIp2Exception e) {
                e.printStackTrace();
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }
}