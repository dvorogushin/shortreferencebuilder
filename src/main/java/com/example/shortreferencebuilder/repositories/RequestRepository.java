package com.example.shortreferencebuilder.repositories;

import com.example.shortreferencebuilder.models.RequestInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Request's JPA repository.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public interface RequestRepository extends JpaRepository<RequestInfo, Long> {

}
