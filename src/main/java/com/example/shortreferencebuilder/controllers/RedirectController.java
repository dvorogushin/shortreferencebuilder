package com.example.shortreferencebuilder.controllers;

import com.example.shortreferencebuilder.services.RedirectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

/**
 * Date: 22.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Main controller resolves redirection from short link to URL.
 * Get method available only.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@RequiredArgsConstructor
@Controller
public class RedirectController {

    /**
     * Service component, redirect resolver's object.
     */
    private final RedirectService redirectService;

    /**
     * Redirects a request to corresponding URL.
     *
     * @param reference Short reference name.
     * @param request   redirecting request.
     * @return redirection to URL.
     */
    @GetMapping("/{reference}")
    public String redirect(
            @PathVariable("reference") String reference,
            HttpServletRequest request) {
        String url = redirectService.getUrl(reference, request);
        return "redirect:" + url;
    }
}
