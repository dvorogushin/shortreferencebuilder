package com.example.shortreferencebuilder.controllers;

import com.example.shortreferencebuilder.repositories.GeoLocationRepository;
import com.example.shortreferencebuilder.services.impl.IpResolver;
import com.example.shortreferencebuilder.transfer.geolocationDto.GeoLocationDto;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

/**
 * Date: 22.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Controller returns CV file.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@RestController
@RequiredArgsConstructor
public class CVController {

    /**
     * CV file storage folder.
     */
    @Value("${cv.folder.path}")
    public String folderPath;

    /**
     * CV file name.
     */
    @Value("${cv.file.name}")
    public String fileName;
    @Autowired
    GeoLocationRepository locationRepository;


    /**
     * Returns CV file in response body.
     *
     * @param response
     * @param request
     */
    @GetMapping("/cv")
    public void getCvFile(HttpServletResponse response, HttpServletRequest request) {
        String localisedFilename = fileName;
        GeoLocationDto geoLocationDto = locationRepository.findGeoByIP(IpResolver.getRequestIp(request));
        if (geoLocationDto != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            System.out.printf("%s CV was downloaded from %s, %s (%s)\n",
                    simpleDateFormat.format(new Date()),
                    geoLocationDto.getCity(),
                    geoLocationDto.getCountry(),
                    request.getRemoteAddr());
            if (geoLocationDto.getCountry().equals("Russia")) {
                String[] chunks = fileName.split("[.]");
                localisedFilename = chunks[0]
                        .concat("_ru.")
                        .concat(chunks[1]);
            }
        }
        Resource resource = new ClassPathResource(folderPath + localisedFilename);
        try {
            InputStream inputStream = resource.getInputStream();
            response.setContentType("application/pdf");
            response.setContentLengthLong(resource.contentLength());
            response.setHeader("Content-Disposition", "filename=\"" + fileName + "\"");
            inputStream.transferTo(response.getOutputStream());
//            IOUtils.copy(inputStream, response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
