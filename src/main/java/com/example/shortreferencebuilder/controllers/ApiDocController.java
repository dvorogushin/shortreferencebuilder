package com.example.shortreferencebuilder.controllers;

import com.ctc.wstx.io.ReaderSource;
import com.example.shortreferencebuilder.exceptions.FileNotFoundException;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Date: 05.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Controller serves API documentation.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Controller
@RequiredArgsConstructor
public class ApiDocController {

    /**
     * Storage folder for specification yaml file.
     */
    @Value("${openApiDoc.folder.path}")
    public String folderPath;

    /**
     * Specification yaml file name.
     */
    @Value("${openApiDoc.file.name.YAML}")
    public String apiFileNameYaml;

    /**
     * Specification json file name.
     */
    @Value("${openApiDoc.file.name.JSON}")
    public String apiFileNameJson;

    /**
     * Static Swagger UI page. Loads specification file and allows test the REST API server.
     *
     * @return HTML swagger UI page.
     */
    @GetMapping("/api/v3/swagger-ui")
    public String getSwaggerUI() {
        return "swagger-ui";
    }

    /**
     * Returns specification file in yaml or json format.
     * Returns json file by default.
     *
     * @param response json or yaml file
     */
    @GetMapping("/api/v3/{api-file}")
    public void getOpenApiDocFile(@PathVariable("api-file") String fileName,
                                  HttpServletResponse response) {
        // default - JSON
        if (fileName.equals("openapi")) {
            fileName = new StringBuilder().append(fileName).append(".json").toString();
        }

        //Path filePath = null;
        Resource resource = null;
        if (fileName.equals("openapi.json")) {
            //filePath = Paths.get(folderPath).resolve(apiFileNameJson);
            resource = new ClassPathResource(folderPath + apiFileNameJson);
            response.setContentType("application/json");
        } else if (fileName.equals("openapi.yaml")){
            //filePath = Paths.get(folderPath).resolve(apiFileNameYaml);
            resource = new ClassPathResource(folderPath + apiFileNameYaml);
            response.setContentType("text/x-yaml");
        } else throw new FileNotFoundException();
        InputStream inputStream = null;
//        try {
//            response.setContentLength((int)Files.size(filePath));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        response.setHeader("Content-Disposition", "filename=\"" + fileName + "\"");
        try {
            inputStream = resource.getInputStream();
            IOUtils.copy(inputStream, response.getOutputStream());
          //  Files.copy(filePath, response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
