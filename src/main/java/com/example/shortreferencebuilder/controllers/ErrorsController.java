package com.example.shortreferencebuilder.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Date: 22.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Controller handles with error pages.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Controller
public class ErrorsController {

    @GetMapping("error/400")
    public String get400Page() {
        return "errors/error_400";
    }

    @GetMapping("error/403")
    public String get403Page() {
        return "errors/error_403";
    }

    @GetMapping("error/404")
    public String get404Page() {
        return "errors/error_404";
    }

    // Перехват всех GET запросов в POST зоне
    @GetMapping("error/405")
    public String get405Page() {
        return "redirect:/user";
    }

    @GetMapping("error/500")
    public String get500Page() {
        return "errors/error_500";
    }
}

