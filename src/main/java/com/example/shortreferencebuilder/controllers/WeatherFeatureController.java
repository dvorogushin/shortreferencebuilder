package com.example.shortreferencebuilder.controllers;

import com.example.shortreferencebuilder.services.WeatherFeatureService;
import com.example.shortreferencebuilder.services.impl.IpResolver;
import com.example.shortreferencebuilder.transfer.geolocationDto.GeoLocationDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Optional;

/**
 * Date: 01.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Controller to deal with Yandex.Weather feature on main page.
 * GET method is supported only.
 * In depending on received {@code cookies} returns different text inside response body.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/weather")
public class WeatherFeatureController {

    /**
     * Service component.
     */
    private final WeatherFeatureService weatherFeatureService;

    /**
     * Reads {@code cookies} and provides with suitable text inside the response body.
     *
     * @param request  supported by Spring.
     * @param response supported by Spring.
     * @return {@link ResponseEntity} with text.
     */
    @GetMapping
    public ResponseEntity<String> weather(
            HttpServletRequest request, HttpServletResponse response) {
        String weatherText = "";
        String remoteIp = IpResolver.getRequestIp(request);
        String weatherStep = "";
        Optional<Cookie> weatherCookie = Arrays.stream(request
                        .getCookies())
                .filter(c -> c.getName().equals("weather"))
                .findFirst();
        if (weatherCookie.isPresent()) {

            switch (weatherCookie.get().getValue()) {
                case "1":
                    weatherStep = "2";
                    GeoLocationDto geoLocationDto = weatherFeatureService.getGeoLocation(remoteIp);
                    String longitude = String.valueOf(geoLocationDto.getLongitude());
                    String latitude = String.valueOf(geoLocationDto.getLatitude());
                    weatherText = "Потому что такая температура в YandexWeather по координатам: " +
                            longitude + ", " + latitude + ". С чего вдруг тут эти координаты?";
                    Cookie cookieLon = new Cookie("lon", longitude);
                    Cookie cookieLat = new Cookie("lat", latitude);
                    response.addCookie(cookieLat);
                    response.addCookie(cookieLon);
                    break;
                case "2":
                    weatherStep = "3";
                    weatherText = "Потому что именно такие координаты соответствуют IP " +
                            remoteIp +
                            " в базе данных GeoLite2. С чего тут этот IP?";
                    break;
                case "3":
                    weatherStep = "4";
                    weatherText = "Это твой публичный IP.";
                    break;
                default:
                    weatherStep = "1";
                    weatherText = "Есть мнение, что за окном "
                            + weatherFeatureService.getWeatherTemp(remoteIp)
                            + " градусов. ";
            }
        } else {
            weatherStep = "1";
            weatherText = "Есть мнение, что за окном "
                    + weatherFeatureService.getWeatherTemp(remoteIp)
                    + "градусов. ";
        }

        weatherText = "{\"text\": \"" + weatherText + "\"}";


        Cookie cookie = new Cookie("weather", weatherStep);
        response.addCookie(cookie);
        return ResponseEntity.ok(weatherText);
    }
}
