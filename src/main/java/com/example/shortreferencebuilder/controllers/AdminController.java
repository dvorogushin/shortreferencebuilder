package com.example.shortreferencebuilder.controllers;

import com.example.shortreferencebuilder.models.Reference;
import com.example.shortreferencebuilder.models.RequestInfo;
import com.example.shortreferencebuilder.models.User;
import com.example.shortreferencebuilder.services.ReferenceService;
import com.example.shortreferencebuilder.services.UserService;
import com.example.shortreferencebuilder.transfer.CookieInfoDto;
import com.example.shortreferencebuilder.transfer.ReferenceDto;
import com.example.shortreferencebuilder.transfer.RequestInfoDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Date: 26.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Controller for "{@code /admin/**}" location. Provides access to user's account with {@code ADMIN} role.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Controller
@RequiredArgsConstructor
public class AdminController {

    private final UserService userService;
    private final ReferenceService referenceService;

    /**
     * GET all
     * <p>Admin's account page. Transfers all {@code references} to the model.
     *
     * @param model
     * @param userId User's ID, provided by SpringSecurity.
     * @return HTML page, rendered by FreeMarker.
     */
    @GetMapping("/admin")
    public String getUserPage(Model model,
                              @AuthenticationPrincipal(expression = "id") Long userId) {
        User user = userService.getUser(userId);
        model.addAttribute("user", user);
        List<Reference> references = referenceService.getAll();
        model.addAttribute("references", ReferenceDto.from(references));
        return "user";
    }

    /**
     * POST
     * <p>Works as a GET method, doesn't change any entity.
     * Shows {@code requests} for certain reference only.
     * Transfers the {@code reference} and its {@code requests} to the model.</p>
     *
     * @param model
     * @param shortReference Short reference name.
     * @param userId         User's ID, provided by SpringSecurity.
     * @return HTML page, rendered by FreeMarker.
     */
    @PostMapping("/admin/requests")
    public String getReferencePage(Model model,
                                   @RequestParam(required = false, name = "reference") String shortReference,
                                   @AuthenticationPrincipal(expression = "id") Long userId) {
        User user = userService.getUser(userId);
        model.addAttribute("user", user);

        // TODO check parameter presence

        List<RequestInfo> requests = referenceService.getRequests(user, shortReference);
        model.addAttribute("requests", RequestInfoDto.from(requests));

        Reference reference = referenceService.getReference(shortReference);
        model.addAttribute("reference", ReferenceDto.from(reference));

        return "user";
    }

    /**
     * POST
     * <p>Works as a GET method, doesn't change any entity.
     * Shows {@code cookies} for certain {@code request} only.
     * Transfers the {@code reference}, {@code request} and its {@code cookies} to the model.</p>
     *
     * @param model
     * @param requestId      Request's ID
     * @param shortReference Short reference name.
     * @param userId         User's ID, provided by SpringSecurity.
     * @return HTML page, rendered by FreeMarker.
     */
    @PostMapping("/admin/cookies")
    public String getCookiePage(Model model,
                                @RequestParam(required = false, name = "request") Long requestId,
                                @RequestParam(required = false, name = "reference") String shortReference,
                                @AuthenticationPrincipal(expression = "id") Long userId) {
        User user = userService.getUser(userId);
        model.addAttribute("user", user);
        // todo проверка авторизации и наличия параметра

        Reference reference = referenceService.getReference(shortReference);
        model.addAttribute("reference", ReferenceDto.from(reference));

        RequestInfo requestInfo = referenceService.getRequest(user, requestId);
        model.addAttribute("request", RequestInfoDto.from(requestInfo));

        model.addAttribute("cookies", CookieInfoDto.from(requestInfo.getCookies()));

        return "user";
    }

    /**
     * POST
     * <p>Payment page.</p>
     * Transfers the {@code reference} and payment conditions to the model.</p>
     *
     * @param model
     * @param shortReference Short reference name.
     * @param userId         User's ID, provided by SpringSecurity.
     * @return HTML page, rendered by FreeMarker.
     */
    @PostMapping("admin/buy")
    public String getPaymentPage(Model model,
                                 @RequestParam("reference") String shortReference,
                                 @AuthenticationPrincipal(expression = "id") Long userId) {
        User user = userService.getUser(userId);
        model.addAttribute("user", user);

        Reference reference = referenceService.getReference(shortReference);
        model.addAttribute("reference", ReferenceDto.from(reference));

        model.addAttribute("payPeriod", LocalDateTime.now().plusYears(1));
        model.addAttribute("payPrice", "10 RUR");

        return "user";
    }

    /**
     * GET
     * <p>Payment operation. Used to set the owner of the reference, change the state, fix the payment/buying.</p>
     * Redirects to admin's account.
     * @param model
     * @param shortReference Short reference name.
     * @param userId         User's ID, provided by SpringSecurity.
     * @return redirects to admin's account page.
     */
    @GetMapping("admin/pay")
    public String paymentOperation(Model model,
                                   @RequestParam("reference") String shortReference,
                                   @AuthenticationPrincipal(expression = "id") Long userId) {
        User user = userService.getUser(userId);
        model.addAttribute("user", user);

        referenceService.buyReference(user, shortReference);

        return "redirect:/admin";
    }

    /**
     * POST
     * <p>Deleting operation. Deletes the reference, redirects to admin's account page.</p>
     * @param model
     * @param shortReference Short reference name.
     * @param userId         User's ID, provided by SpringSecurity.
     * @return redirects to admin's account page.
     */
    @PostMapping("admin/delete")
    public String deleteReference(Model model,
                                  @RequestParam("reference") String shortReference,
                                  @AuthenticationPrincipal(expression = "id") Long userId) {
        User user = userService.getUser(userId);
        model.addAttribute("user", user);

        referenceService.deleteReference(user, shortReference);

        return "redirect:/admin";
    }

    /**
     * POST
     * <p>Change state operation. Redirects to admin's page with {@code requests}</p>
     * @param model
     * @param shortReference Short reference name.
     * @param userId         User's ID, provided by SpringSecurity.
     * @param request
     * @return redirects to admin's account page.
     */
    @PostMapping("admin/changestate")
    public ModelAndView changeState(ModelMap model,
                                    @RequestParam("reference") String shortReference,
                                    @AuthenticationPrincipal(expression = "id") Long userId,
                                    HttpServletRequest request) {
        User user = userService.getUser(userId);
        referenceService.changeState(user, shortReference);

        // redirect from POST to POST
        request.setAttribute(
                View.RESPONSE_STATUS_ATTRIBUTE, HttpStatus.TEMPORARY_REDIRECT);
        return new ModelAndView("redirect:/admin/requests");
    }
}
