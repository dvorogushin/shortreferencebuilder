package com.example.shortreferencebuilder.controllers;

import com.example.shortreferencebuilder.config.EnvironmentUtil;
import com.example.shortreferencebuilder.forms.ReferenceForm;
import com.example.shortreferencebuilder.models.Reference;

import static com.example.shortreferencebuilder.models.Reference.ReferenceValidator;

import com.example.shortreferencebuilder.services.ReferenceService;
import com.example.shortreferencebuilder.transfer.ReferenceDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Main controller, serves non authorized zone.
 * Returns default HTML page.
 * Get method available only.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@RequiredArgsConstructor
@Controller
public class ReferenceController {

    private final ReferenceService referenceService;

    /**
     * Returns default page.
     *
     * @param model          provided by Spring.
     * @param authentication provided by SpringSecurity.
     * @return Default main page.
     */
    @GetMapping("/main")
    public String getMainPageGET(Model model,
                                 Authentication authentication) {

        // Check if user has authentication, if true - transfer email to the model
        if (authentication != null) {
            insertUsersEmail(authentication, model);
        }
        model.addAttribute("maxfreeperiod", Reference.DEFAULT_FREE_PERIOD_DAYS);
        return "main";
    }

    /**
     * Returns default page for authenticated user (POST to POST?).
     *
     * @param model          provided by Spring.
     * @param authentication provided by SpringSecurity.
     * @return Default main page.
     */
    @PostMapping("/main")
    public String getMainPagePOST(Model model,
                                  Authentication authentication) {

        // Check if user has authentication, if true - transfer email to the model
        if (authentication != null) {
            insertUsersEmail(authentication, model);
        }
        model.addAttribute("maxfreeperiod", Reference.DEFAULT_FREE_PERIOD_DAYS);
        return "main";
    }

    /**
     * Short link creation. Request is handled as an operation.
     * After successful creation redirects to the {@code reference} page.
     * Otherwise redirects to main page.
     *
     * @param form               {@link ReferenceForm} consists of URL and freePeriod values.
     * @param result             {@link BindingResult} object, result of {@code request's} data validation, as well as result of binding request to {@code ReferenceForm}.
     * @param redirectAttributes supported by Spring, is used to redirect error message between methods.
     * @param request            supported by Spring
     * @return redirects to either new {@code reference} page or default page.
     */
    @PostMapping("/main/create")
    public String createRef(@Valid ReferenceForm form,
                            BindingResult result,
                            RedirectAttributes redirectAttributes,
                            HttpServletRequest request) {

        // If request's data (URL) not valid
        if (result.hasErrors() || form.getUrl() == "") {
            redirectAttributes.addFlashAttribute("error",
                    "Поле не должно быть пустым. " +
                            "Период должен быть от 1 до " +
                            Reference.DEFAULT_FREE_PERIOD_DAYS +
                            " дней.");
            return "redirect:/main";
        }

        // check if redirect URL can be mapped to java.net.URL.class
        // and hasn't the same host name as server's host name
        if (!Reference.validate(ReferenceValidator::isUrlCorrect, form.getUrl(), request)) {
            redirectAttributes.addFlashAttribute("error", "Не корректная ссылка.");
            return "redirect:/main";
        }

        String shortReference = referenceService.createReference(form);
        return "redirect:/reference/" + shortReference;
    }

    // страница ссылки

    /**
     * Returns the short link ({code reference}) page.
     *
     * @param shortReference
     * @param model
     * @param request
     * @param authentication
     * @return
     */
    @GetMapping("/reference/{short-reference}")
    public String getReferencePage(@PathVariable("short-reference") String shortReference,
                                   Model model,
                                   HttpServletRequest request,
                                   Authentication authentication) {

        // Check if user has authentication, if true - transfer email to the model
        if (authentication != null) {
            insertUsersEmail(authentication, model);
        }

        Reference reference = referenceService.getReference(shortReference);
        model.addAttribute("reference", ReferenceDto.from(reference));

        // Localization of short reference
        EnvironmentUtil env = new EnvironmentUtil();
        String localizedReference =
//                env.getScheme() + "://" +
//                        env.getHostName() + "/" +
                        env.getServerUrlPrefi() + "/" + reference.getShortRef();
        model.addAttribute("fullUrl", localizedReference);
        return "reference";
    }

    /**
     * Defines if the user has already authentication.
     * If true - transfers user's email to the model.
     *
     * @param authentication supported by Spring.
     * @param model          supported by Spring.
     */
    public void insertUsersEmail(Authentication authentication, Model model) {
        if (authentication.getPrincipal() instanceof UserDetails) {
            String username = ((UserDetails) authentication.getPrincipal()).getUsername();
            model.addAttribute("username", username);
        } else {
            String username = authentication.getPrincipal().toString();
            model.addAttribute("username", username);
        }
    }
}
