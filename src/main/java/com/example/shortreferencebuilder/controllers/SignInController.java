package com.example.shortreferencebuilder.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * SignIn controller. Returns signin page.
 * GET method is supported only.
 * POST method is interrupted by Spring Security.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Controller
@RequestMapping("/signin")
public class SignInController {

    /**
     * GET signin page.
     *
     * @param model   supported by Spring
     * @param auth    supported by Spring
     * @param request supported by Spring
     * @return HTML signin page, rendered by FreeMarker.
     */
    @GetMapping
    public String getSignInPage(Model model,
                                Authentication auth,
                                HttpServletRequest request) {

        // if user has authenticated already
        if (auth != null) {
            return "redirect:/user";
        }

        // if request has 'error' parameter
        if (request.getParameterMap().containsKey("error")) {
            model.addAttribute("error", true);
        }

        return "signin";
    }
}
