package com.example.shortreferencebuilder.controllers;

import com.example.shortreferencebuilder.forms.SignUpForm;
import com.example.shortreferencebuilder.services.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * SignUn controller. Returns signup page.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/signup")
public class SignUpController {

    private final SignUpService signUpService;

    /**
     * GET method. Returns signup page.
     *
     * @param authentication supported by Spring.
     * @return HTML page, rendered by FreeMarker.
     */
    @GetMapping
    public String getSignUpPage(Authentication authentication) {
        if (authentication != null) {
            return "/";
        }
        return "signup";
    }

    /**
     * POST method. Receives data from {@code signup form}.
     * Is used as a result of operation.
     * If registration is successful then redirects to signin page.
     * Otherwise redirects to signup page again.
     *
     * @param form             {@link SignUpForm} - registration details which are coming from {@code form}.
     * @param result           {@link BindingResult} object, result of {@code request's} data validation, as well as result of binding request to {@code ReferenceForm}.
     * @param forRedirectModel supported by Spring, is used to redirect error message between methods.
     * @return redirects to signin page.
     */
    @PostMapping
    public String signUpUser(@Valid SignUpForm form,
                             BindingResult result,
                             RedirectAttributes forRedirectModel) {

        // validation of signupform data
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Some errors occurred. Pls check the data you trying to send.");
            return "redirect:/signup";
        }

        // check email as unique record
        if (signUpService.doesEmailExist(form)) {
            forRedirectModel
                    .addFlashAttribute("errors",
                            "Пользователь с таким email уже зарегистрирован");
            return "redirect:/signup";
        }

        signUpService.signUpUser(form);
        return "redirect:/signin";
    }
}
