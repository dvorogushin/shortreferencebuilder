package com.example.shortreferencebuilder.security.details;

import com.example.shortreferencebuilder.models.User;
import com.example.shortreferencebuilder.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Date: 22.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * A part of Spring Security. Defines a conditions of user's identification.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    /**
     * User's repository object.
     */
    private final UserRepository userRepository;

    /**
     * Identifies a user by email.
     * Email is a unique user's identifier for any user's {@link User.State state}
     * except state {@link User.State#DELETED DELETED}.
     * Identifier for {@link User.State#DELETED DELETED} accounts is absent.
     * Any quantity of {@link User.State#DELETED DELETED} accounts can have the same email.
     *
     * @param email user's email as string.
     * @return the {@code User} wrapped in {@link UserDetails} object.
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        // all users
        List<User> usersAll = userRepository.findAllByEmail(email);

        // not deleted users
        List<User> usersNotDeleted = usersAll.stream()
                .filter(u -> u.getState() != (User.State.DELETED))
                .collect(Collectors.toList());

        // unique user
        if (usersNotDeleted.size() == 1) {
            return new UserDetailsImpl(usersNotDeleted.stream()
                    .findFirst()
                    .orElseThrow(() -> new UsernameNotFoundException("User not found")));
        }
        return null;
    }
}
