package com.example.shortreferencebuilder.security.details;

import com.example.shortreferencebuilder.models.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

/**
 * Date: 22.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * A part of Spring Security. Defines user's settings.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@RequiredArgsConstructor
public class UserDetailsImpl implements UserDetails {

    /**
     * User object.
     */
    private final User user;

    /**
     * The user's rights are granted depend on user's Role.
     *
     * @return user's authority.
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = user.getRole().toString();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);
        return Collections.singleton(authority);
    }

    /**
     * Method to get password.
     *
     * @return
     */
    @Override
    public String getPassword() {
        return user.getHashPassword();
    }

    /**
     * Method to get user's name.
     *
     * @return
     */
    @Override
    public String getUsername() {
        return user.getEmail();
    }

    /**
     * Defines expiration conditions for user's account.
     *
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Defines locking conditions for user's account.
     *
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return !user.getState().equals(User.State.BANNED);
    }

    /**
     * Defines the expiration conditions for user's Credentials.
     *
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Defines when a user's account is available.
     *
     * @return
     */
    @Override
    public boolean isEnabled() {
        return user.getState().equals(User.State.CONFIRMED);
    }

    /**
     * Returns user's ID.
     *
     * @return
     */
    public Long id() {
        return user.getId();
    }

    /**
     * Returns {@link User} object.
     *
     * @return
     */
    public User getUser() {
        return user;
    }
}
