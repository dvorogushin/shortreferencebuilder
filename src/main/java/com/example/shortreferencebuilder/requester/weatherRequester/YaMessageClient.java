package com.example.shortreferencebuilder.requester.weatherRequester;

import com.example.shortreferencebuilder.transfer.yaWeatherDto.YaMessageDto;

import java.io.IOException;
import java.net.URI;

/**
 * Date: 03.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Yandex.Weather.API client's interface.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */

public interface YaMessageClient {
    YaMessageDto request(URI uri) throws IOException;
}
