/*
 * Copyright (c) 2002-2022 meteo@m4j.ru
 */
package com.example.shortreferencebuilder.requester.weatherRequester;

import com.example.shortreferencebuilder.transfer.geolocationDto.GeoLocationDto;
import com.example.shortreferencebuilder.transfer.yaWeatherDto.YaMessageDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 * Date: 03.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Yandex.Weather.API requester.
 * Builds a request and invokes {@code request()} method.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Service
@RequiredArgsConstructor
public class YaMessageRequester {

    /**
     * Yandex.Weather.API host.
     */
    @Value("${meteo.provider.host:api.weather.yandex.ru}")
    private String host;

    /**
     * Yandex.Weather.API scheme.
     */
    @Value("${meteo.provider.scheme:http}")
    private String scheme;

    /**
     * Yandex.Weather.API location.
     */
    @Value("${meteo.provider.path:/v2/informers/}")
    private String path;

    /**
     * HTTP client.
     */
    private final YaMessageClient client;

    /**
     * Invokes {@code request()} method of a client.
     *
     * @param geo
     * @return
     */
    public YaMessageDto requestProvider(GeoLocationDto geo) {
        YaMessageDto dto = null;
        try {
            dto = client.request(getUri(geo));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }

    /**
     * Request builder.
     * <p>
     * http://api.weather.yandex.ru/v2/informers?
     * lat=latitude
     * & lon=longitude
     * & [lang=response language]
     * (header) X-Yandex-API-Key: value
     *
     * @param geo {@link GeoLocationDto} object with geographic coordinates.
     * @return built query.
     */
    URI getUri(GeoLocationDto geo) {
        return UriComponentsBuilder.newInstance()
                .scheme(scheme)
                .host(host)
                .path(path)
                .queryParam("lat", geo.getLatitude())
                .queryParam("lon", geo.getLongitude())
                .queryParam("limit", 1)
                .buildAndExpand().toUri();
    }
}
