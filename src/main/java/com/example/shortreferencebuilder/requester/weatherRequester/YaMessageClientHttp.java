package com.example.shortreferencebuilder.requester.weatherRequester;

import com.example.shortreferencebuilder.transfer.yaWeatherDto.YaMessageDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * Date: 03.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * HTTP client.
 * Sends a GET request to the Yandex.Weather.API server.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Component
@RequiredArgsConstructor
public class YaMessageClientHttp implements YaMessageClient {

    /**
     * Api key. Is used as token in request's header.
     */
    @Value("${YANDEX_API_KEY}")
    private String apiKey;

    /**
     * JSON mapper.
     */
    private final ObjectMapper jacksonMapper;


    /**
     * Sends request to the Yandex.Weather.API server.
     * Parses and maps response to {@link YaMessageDto} object.
     *
     * @param uri - Yandex.Weather.API uri.
     * @return {@link YaMessageDto} object.
     * @throws InterruptedException
     */
    @Override
    public YaMessageDto request(URI uri) throws IOException {

        YaMessageDto yaMessageDto;
        HttpRequest request;
        request = HttpRequest.newBuilder()
                .uri(uri)
                .header("X-Yandex-API-Key", apiKey)
                .GET()
//                .version(HttpClient.Version.HTTP_2)
                .build();
        try {
            HttpResponse<String> response = HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString());
            yaMessageDto = jacksonMapper.readValue(response.body(), YaMessageDto.class);
            return yaMessageDto;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
