package com.example.shortreferencebuilder.transfer.geolocationDto;

import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * Date: 03.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * DTO object, is used to map a response from GeoLite2 database.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Data
@AllArgsConstructor
@Builder
public class GeoLocationDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Name of country.
     */
    private String country;

    /**
     * Name of city.
     */
    private String city;

    /**
     * Geographic latitude.
     */
    private Double latitude;

    /**
     * Geographic longitude.
     */
    private Double longitude;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
