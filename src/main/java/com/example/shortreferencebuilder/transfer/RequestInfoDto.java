package com.example.shortreferencebuilder.transfer;

import com.example.shortreferencebuilder.models.RequestInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Date: 25.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Converts the {@link RequestInfo request} entity to the {@link RequestInfoDto DTO} object.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Data
@AllArgsConstructor
@Builder
public class RequestInfoDto {

    /**
     * {@code Request's} ID.
     */
    private Long id;

    /**
     * {@code Request's} date and time of creation.
     */
    private LocalDateTime creationDate;

    /**
     * A Requester's host name.
     */
    private String host;

    /**
     * A {@code User-Agent} header value.
     */
    private String userAgent;

    /**
     * A Requester's IP address.
     */
    private String address;

    /**
     * A Requester's host port.
     */
    private String port;

    /**
     * Result of redirection.
     */
    private Boolean wasRedirected;

    /**
     * Quantity of {@link com.example.shortreferencebuilder.models.CookieInfo cookies} in total.
     */
    private Integer cookieCount;

    /**
     * Geographic country, which {@code request's} IP belongs to.
     */
    private String country;

    /**
     * Geographic city, which {@code request's} IP belongs to.
     */
    private String city;

    /**
     * Temperature in degrees at the moment of {@code request}.
     */
    private Integer temperature;

    /**
     * Converts one {@link RequestInfo request} item to {@link RequestInfoDto DTO} object.
     *
     * @param requestInfo {@link RequestInfo} object.
     * @return {@link RequestInfoDto} object.
     */
    public static RequestInfoDto from(RequestInfo requestInfo) {
        return RequestInfoDto.builder()
                .id(requestInfo.getId())
                .creationDate(requestInfo.getCreationDateTime())
                .host(requestInfo.getHost())
                .userAgent(requestInfo.getUserAgent())
                .address(requestInfo.getAddress())
                .port(String.valueOf(requestInfo.getPort()))
                .wasRedirected(requestInfo.getRedirected())
                .cookieCount(requestInfo.getCookies().size())
                .city(requestInfo.getCity())
                .country(requestInfo.getCountry())
                .temperature(requestInfo.getTemperature())
                .build();
    }

    /**
     * Converts collection of requests.
     * @param requestInfos List of {@link RequestInfo}.
     * @return List of {@link RequestInfoDto} objects.
     */
    public static List<RequestInfoDto> from(List<RequestInfo> requestInfos) {
        return requestInfos.stream().map(RequestInfoDto::from).collect(Collectors.toList());
    }
}
