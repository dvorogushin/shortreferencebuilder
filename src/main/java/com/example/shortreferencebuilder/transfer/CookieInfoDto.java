package com.example.shortreferencebuilder.transfer;

import com.example.shortreferencebuilder.models.CookieInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Date: 25.01.2022
 * Project: shortreferencebuilder
 * Converts the {@link CookieInfo cookie} entity to the {@link CookieInfoDto DTO} object.
 *
 * @author Dmitry Vorogushin (@dvorogushin)
 */
@Data
@AllArgsConstructor
@Builder
public class CookieInfoDto {

    /**
     * Cookie's name.
     */
    private String name;

    /**
     * Cookie's value.
     */
    private String value;

    /**
     * Converts one {@link CookieInfo cookie} item.
     * @param cookieInfo Cookie's entity object.
     * @return {@link CookieInfoDto} object.
     */
    public static CookieInfoDto from(CookieInfo cookieInfo) {
        return CookieInfoDto.builder()
                .name(cookieInfo.getName())
                .value(cookieInfo.getValue())
                .build();
    }

    /**
     * Converts collection of Cookies.
     * @param cookieInfos List of {@code Cookies}.
     * @return List of {@link CookieInfoDto} objects.
     */
    public static List<CookieInfoDto> from(List<CookieInfo> cookieInfos) {
        return cookieInfos.stream().map(CookieInfoDto::from).collect(Collectors.toList());
    }

}
