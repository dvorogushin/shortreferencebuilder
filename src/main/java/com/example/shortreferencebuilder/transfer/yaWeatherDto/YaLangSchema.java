package com.example.shortreferencebuilder.transfer.yaWeatherDto;

import lombok.*;

/**
 * Date: 03.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Yandex.Weather.API - {@code Lang} object.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Setter
@Getter
@Builder
public class YaLangSchema {

    public static enum schema {

//    “ru_RU” — Russian for the Russia locale.
//    “ru_UA” — Russian for the Ukraine locale.
//    “uk_UA” — Ukrainian for the Ukraine locale.
//    “be_BY” — Belarusian for the Belarus locale.
//    “kk_KZ” — Kazakh for the Kazakhstan locale.
//    “tr_TR” — Turkish for the Turkey locale.
//    “en_US” — International English.

        ru_RU, ru_UA, uk_UA, be_BY, kk_KZ, tr_TR, en_US;
    }
}
