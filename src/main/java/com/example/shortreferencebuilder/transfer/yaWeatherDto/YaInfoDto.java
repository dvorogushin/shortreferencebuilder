/*
 * Copyright (c) 2002-2022 meteo@m4j.ru
 */
package com.example.shortreferencebuilder.transfer.yaWeatherDto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * Date: 03.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Yandex.Weather.API - {@code Info} object.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Getter
@Setter
@EqualsAndHashCode
public class YaInfoDto implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * lat
     * Широта (в градусах).
     * Число
     */
    private Double lat;
    /**
     * lon
     * Долгота (в градусах).
     * Число
     */
    private Double lon;
    /**
     * url
     * Страница населенного пункта на сайте Яндекс.Погода.
     * Строка
     */
    private String url;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
