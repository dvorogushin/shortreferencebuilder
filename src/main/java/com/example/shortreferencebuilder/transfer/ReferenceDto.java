package com.example.shortreferencebuilder.transfer;

import com.example.shortreferencebuilder.config.EnvironmentUtil;
import com.example.shortreferencebuilder.models.Reference;
import com.example.shortreferencebuilder.models.RequestInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Date: 25.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Converts the {@link Reference reference} entity to the {@link ReferenceDto DTO} object.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Data
@AllArgsConstructor
@Builder
public class ReferenceDto {

    /**
     * {@code Reference's} ID.
     */
    private Long id;

    /**
     * {@code Reference's} short name.
     */
    private String shortReference;

    /**
     * {@code Reference's} redirect URL.
     */
    private String redirectReference;

    /**
     * Date and time of {@code reference} creation.
     */
    private LocalDateTime creationDate;

    /**
     * Date ant time of {@code reference} expiration.
     */
    private LocalDateTime expirationDate;

    /**
     * {@code Reference's} state.
     */
    private String state;

    /**
     * Quantity of requests in total.
     */
    private Integer requestCount;

    /**
     * Quantity of redirects (successful requests) in total.
     */
    private Integer redirectCount;

    /**
     * Expiration flag.
     */
    private Boolean isExpired;

    /**
     * {@link com.example.shortreferencebuilder.models.User User}, who owns the {@code reference}.
     */
    private String ownerEmail;

    /**
     * Environment details.
     */
    private static final EnvironmentUtil invUtil = new EnvironmentUtil();

    /**
     * Converts one {@link Reference} item to {@link ReferenceDto DTO} object.
     *
     * @param reference {@link Reference} object.
     * @return {@link ReferenceDto} object.
     */
    public static ReferenceDto from(Reference reference) {
        return ReferenceDto.builder()
                .shortReference(reference.getShortRef())
                .redirectReference(reference.getRedirectRef())
                .creationDate(reference.getCreationDateTime())
                .expirationDate(reference.getExpirationDateTime())
                .state(reference.getState().toString())
                .requestCount(reference.getRequests().size())
                .redirectCount((int) reference.getRequests().stream()
                        .filter(RequestInfo::getRedirected)
                        .count())
                .isExpired(Reference.ReferenceValidator.isExpired(reference))
                .ownerEmail(reference.getOwnerUser() != null ? reference.getOwnerUser().getEmail() : "")
                .build();
    }

    /**
     * Converts list of {@link Reference} objects to the list of {@link ReferenceDto DTO} objects.
     *
     * @param references List of {@link Reference} objects.
     * @return List of {@link ReferenceDto} objects.
     */
    public static List<ReferenceDto> from(List<Reference> references) {
        return references.stream().map(ReferenceDto::from).collect(Collectors.toList());
    }
}
