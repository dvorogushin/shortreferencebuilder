package com.example.shortreferencebuilder.services;

import com.example.shortreferencebuilder.transfer.geolocationDto.GeoLocationDto;

/**
 * Date: 03.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public interface WeatherFeatureService {

    String getWeatherTemp(String ip);

    GeoLocationDto getGeoLocation(String ip);
}
