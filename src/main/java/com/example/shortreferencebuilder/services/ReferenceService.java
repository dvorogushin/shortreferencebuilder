package com.example.shortreferencebuilder.services;

import com.example.shortreferencebuilder.forms.ReferenceForm;
import com.example.shortreferencebuilder.models.Reference;
import com.example.shortreferencebuilder.models.RequestInfo;
import com.example.shortreferencebuilder.models.User;

import java.util.List;

/**
 * Date: 23.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Main service interface to deal with {@code reference} and {@code request} entities.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public interface ReferenceService {
    String createReference(ReferenceForm form);

    Reference getReference(String shortRefernce);

    List<Reference> getAll(User user);
    List<Reference> getAll();

    List<RequestInfo> getRequests(User user, String shortReference);

    RequestInfo getRequest(User user, Long requestId);

    void buyReference(User user, String shortReference);

    void deleteReference(User user, String shortReference);

    void changeState(User user, String shortReference);

}
