package com.example.shortreferencebuilder.services;


import javax.servlet.http.HttpServletRequest;

/**
 * Date: 23.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Is used only for redirection requests.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public interface RedirectService {

    String getUrl(String reference, HttpServletRequest request);

}
