package com.example.shortreferencebuilder.services;


import com.example.shortreferencebuilder.forms.SignUpForm;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */

public interface SignUpService {
    void signUpUser (SignUpForm form);

    boolean doesEmailExist(SignUpForm form);
}
