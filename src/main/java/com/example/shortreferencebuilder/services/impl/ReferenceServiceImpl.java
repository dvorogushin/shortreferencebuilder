package com.example.shortreferencebuilder.services.impl;

import com.example.shortreferencebuilder.exceptions.ReferenceNotFoundException;
import com.example.shortreferencebuilder.exceptions.RequestNotFoundException;
import com.example.shortreferencebuilder.forms.ReferenceForm;
import com.example.shortreferencebuilder.models.Reference;

import com.example.shortreferencebuilder.models.RequestInfo;
import com.example.shortreferencebuilder.models.User;
import com.example.shortreferencebuilder.repositories.ReferenceRepository;
import com.example.shortreferencebuilder.repositories.RequestRepository;
import com.example.shortreferencebuilder.services.ReferenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.shortreferencebuilder.models.Reference.*;

/**
 * Date: 23.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Service implementation class for operations with the {@code references} entity.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Service
@RequiredArgsConstructor
public class ReferenceServiceImpl implements ReferenceService {

    /**
     * {@code Reference} repository object.
     */
    public final ReferenceRepository referenceRepository;

    /**
     * {@code Request} repository object.
     */
    public final RequestRepository requestRepository;

    /**
     * Finds the {@code reference} by name.
     * Checks expiration date.
     *
     * @param shortReference name of desired {@code reference}.
     * @return {@link Reference} object.
     * @throws ReferenceNotFoundException if {@code reference's} name is absent.
     */
    @Override
    public Reference getReference(String shortReference) {

        Reference reference = referenceRepository
                .findByShortRefOrderByIdAsc(shortReference)
                .orElseThrow(ReferenceNotFoundException::new);
        return lazyExpirationControl(reference);
    }

    /**
     * Finds and returns all references owned by {@code user}.
     * All founded references are checked for an expiration in {@link ReferenceServiceImpl#lazyExpirationControl} method.
     * Final collection is sorting by {@code reference} ID.
     *
     * @param user {@link User} object.
     * @return Collection of {@link Reference} objects.
     */
    @Override
    public List<Reference> getAll(User user) {

        List<Reference> references = referenceRepository.findAllByOwnerUserOrderById(user);

        return references.stream()
                .map(this::lazyExpirationControl)
                .filter(r -> r.getOwnerUser() != null && r.getOwnerUser().equals(user))
                .collect(Collectors.toList());
    }

    /**
     * Finds and returns all references from {@link ReferenceServiceImpl#referenceRepository}.
     * All founded references are checked for an expiration in {@link ReferenceServiceImpl#lazyExpirationControl} method.
     * Final collection is sorting by {@code reference} ID.
     *
     * @return Collection of {@link Reference} objects.
     */
    @Override
    public List<Reference> getAll() {

        List<Reference> references = referenceRepository.findAll();
        references.stream().forEach(r -> lazyExpirationControl(r));
        return references.stream()
                .sorted(Comparator.comparing(Reference::getId))
                .collect(Collectors.toList());
    }

    /**
     * Finds and returns all requests to the {@code reference} for redirection.
     * User should either be an owner of the reference or has {@link User.Role role} `ADMIN`.
     * Final collection is sorting by {@code request} ID.
     *
     * @param shortReference short name of the {@code reference}.
     * @param user           {@link User} object.
     * @return Collection of {@link RequestInfo} objects.
     */
    @Override
    public List<RequestInfo> getRequests(User user, String shortReference) {
        Reference reference = getReference(shortReference);
        // if user is owner or has ADMIN role
        if (validate(ReferenceValidator::isUserHasAuthoritiesToGetReference, reference, user)) {
            return reference.getRequests().stream()
                    .sorted(Comparator.comparing(RequestInfo::getId))
                    .collect(Collectors.toList());
        }
        // TODO обработать null


        return null;
    }

    /**
     * Finds a {@code request} by ID. Used to get {@code Cookies}.
     * User should either be an owner of the reference or has {@link User.Role role} `ADMIN`.
     *
     * @param requestId ID of the {@code request}.
     * @param user      {@link User} object.
     * @return {@link RequestInfo} object.
     * @throws RequestNotFoundException if ID is absent.
     */
    @Override
    public RequestInfo getRequest(User user, Long requestId) {
        RequestInfo requestInfo = requestRepository
                .findById(requestId)
                .orElseThrow(RequestNotFoundException::new);
        // if user is owner or has ADMIN role
        if (validate(ReferenceValidator::isUserHasAuthoritiesToGetReference, requestInfo.getReference(), user)) {
            return requestInfo;
        }
        // todo обработать null
        return null;
    }

    /**
     * Operation - purchase the {@code reference}.
     * Sets the user as reference owner and switches the status of the {@code reference} on {@code ACTIVE} state.
     *
     * @param shortReference short name of the desired {@code reference}.
     * @param user           {@link User} object.
     * @return {@link Reference} object.
     */
    @Override
    public void buyReference(User user, String shortReference) {
        // TODO проверить не куплена ли ссылка уже
        Reference reference = getReference(shortReference);
        reference.setOwnerUser(user);
        reference.setState(State.ACTIVE);
        reference.setExpirationDateTime(LocalDateTime.now().plusYears(1));
        referenceRepository.save(reference);
    }

    /**
     * Deletes the {@code reference}.
     * User should either be an owner of the reference or has {@link User.Role role} `ADMIN`.
     *
     * @param shortReference short name of the desired reference.
     * @param user           {@link User} object.
     */
    @Override
    public void deleteReference(User user, String shortReference) {

        Reference reference = getReference(shortReference);

        // if user is owner or has ADMIN role
        if (validate(ReferenceValidator::isUserHasAuthoritiesToGetReference, reference, user)) {
            referenceRepository.delete(reference);
        }
    }

    /**
     * Changes the status of the {@code reference} between {@code ACTIVE} and {@code NOT_ACTIVE} states.
     * User should either be an owner of the reference or has {@link User.Role role} `ADMIN`.
     *
     * @param shortReference short name of the desired {@code reference}.
     * @param user           - {@link User} object.
     * @return {@link Reference} object.
     */
    @Override
    public void changeState(User user, String shortReference) {

        Reference reference = getReference(shortReference);

        if (reference.getExpirationDateTime().isBefore(LocalDateTime.now())) return;
        // if user is owner or has ADMIN role
        if (validate(ReferenceValidator::isUserHasAuthoritiesToGetReference, reference, user)) {
            if (reference.getState().equals(Reference.State.NOT_ACTIVE)) {
                reference.setState(Reference.State.ACTIVE);
            } else {
                reference.setState(Reference.State.NOT_ACTIVE);
            }
            referenceRepository.save(reference);
        }
    }

    /**
     * Creates new {@code reference}.
     *
     * @param form {@link ReferenceForm} object (URL and {@code freePeriod}).
     * @return {@link Reference} object.
     */
    @Override
    public String createReference(ReferenceForm form) {

        Reference reference = Reference.builder()
                .creationDateTime(LocalDateTime.now())
                .expirationDateTime(LocalDateTime.now().plusDays(form.getFreePeriod()))
                .redirectRef(form.getUrl())
                .shortRef(Reference.create(ReferenceCreator::createShortReference))
                .state(Reference.State.ACTIVE)
                .build();

        referenceRepository.save(reference);
        return reference.getShortRef();
    }

    /**
     * Checks expiration date of the {@code reference}.
     * If expiration date has passed than state is changed to a {@code NOT_ACTIVE} value and owner became {@code null}.
     *
     * @param reference {@link Reference} object.
     * @return The same {@link Reference} object.
     */
    public Reference lazyExpirationControl(Reference reference) {
        if (Reference.validate(ReferenceValidator::isExpired, reference)) {
            reference.setState(Reference.State.NOT_ACTIVE);
            reference.setOwnerUser(null);
            referenceRepository.save(reference);
        }
        return reference;
    }
}
