package com.example.shortreferencebuilder.services.impl;

import com.example.shortreferencebuilder.forms.SignUpForm;
import com.example.shortreferencebuilder.models.User;
import com.example.shortreferencebuilder.repositories.UserRepository;
import com.example.shortreferencebuilder.services.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * SignUp Service implementation class.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    /**
     * Saves user's account.
     * <p>Default values:
     * <ul>
     *     <li>{@link User.Role role} - `User`</li>
     *     <li>{@link User.State state} - `Confirmed`</li>
     * </ul>
     *
     * @param form {@link SignUpForm} object, consists of browser's signup form parameters.
     */
    @Override
    public void signUpUser(SignUpForm form) {

        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName((form.getLastName()))
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .creationDateTime(LocalDateTime.now())
                .role(User.Role.USER)
                .state(User.State.CONFIRMED)
                .build();
        userRepository.save(user);
    }

    /**
     * Checks if the email exists.
     * Checking are made between not deleted accounts.
     *
     * @param form {@link SignUpForm} object, consists of browser's signup form parameters.
     * @return true if email already exists.
     */
    @Override
    public boolean doesEmailExist(SignUpForm form) {
        Boolean result = true;
        List<User> users = userRepository.findAllByEmail(form.getEmail());
        if (users.stream()
                .filter(u -> u.getState() != (User.State.DELETED))
                .count() == 0) {
            result = false;
        };
        return result;
    }
}
