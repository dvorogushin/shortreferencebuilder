package com.example.shortreferencebuilder.services.impl;

import com.example.shortreferencebuilder.exceptions.ReferenceNotFoundException;
import com.example.shortreferencebuilder.models.CookieInfo;
import com.example.shortreferencebuilder.models.Reference;
import com.example.shortreferencebuilder.models.RequestInfo;
import com.example.shortreferencebuilder.repositories.CookieRepository;
import com.example.shortreferencebuilder.repositories.GeoLocationRepository;
import com.example.shortreferencebuilder.repositories.ReferenceRepository;
import com.example.shortreferencebuilder.repositories.RequestRepository;
import com.example.shortreferencebuilder.requester.weatherRequester.YaMessageRequester;
import com.example.shortreferencebuilder.services.RedirectService;
import com.example.shortreferencebuilder.transfer.geolocationDto.GeoLocationDto;
import com.example.shortreferencebuilder.transfer.yaWeatherDto.YaMessageDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;

import static com.example.shortreferencebuilder.models.Reference.ReferenceValidator;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Defines redirection logic. Checks the user's rights and the {@code reference} state.
 * Saves a request's details.
 * <p>Gets and saves in parallel thread:
 * <ul>
 *     <li>a geographic coordinates in GeoLite2 database</li>
 *     <li>a weather conditions in Yandex.Weather.API</li>
 * </ul>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Service
@RequiredArgsConstructor
public class RedirectServiceImpl implements RedirectService {

    /**
     * {@code Reference} repository implementation.
     */
    public final ReferenceRepository referenceRepository;

    /**
     * {@code Request} repository implementation.
     */
    public final RequestRepository requestRepository;

    /**
     * {@code Cookie} repository implementation.
     */
    public final CookieRepository cookieRepository;

    /**
     * GeoLocation repository implementation.
     */
    public final GeoLocationRepository locationRepository;

    /**
     * Yandex.weather requester implementation.
     */
    private final YaMessageRequester requester;

    /**
     * Main redirecting method. Gets short link, returns final URL.
     *
     * @param shortReference short link's name.
     * @param servletRequest supported by Spring.
     * @return URL as string.
     */
    @Override
    public String getUrl(String shortReference, HttpServletRequest servletRequest) {

        String url;

        // short link's validation
        if (!Reference.validate(ReferenceValidator::checkChars, shortReference)) {
            throw new ReferenceNotFoundException();
        }

        // find the short link
        Reference reference = referenceRepository
                .findByShortRefOrderByIdAsc(shortReference)
                .orElseThrow(ReferenceNotFoundException::new);

        // save request's details
        RequestInfo requestInfo = RequestInfo.builder()
                .creationDateTime(LocalDateTime.now())
                .address(servletRequest.getRemoteAddr())
                .port(servletRequest.getRemotePort())
                .host(servletRequest.getHeader("Host"))
                .userAgent(servletRequest.getHeader("User-Agent"))
                .reference(reference)
                .redirected(false)
                .build();
        requestRepository.save(requestInfo);

        // save request's cookies
        Cookie[] cookies = servletRequest.getCookies();
        if (cookies != null) {
            Arrays.stream(cookies).map(c -> {
                return CookieInfo.builder()
                        .name(c.getName())
                        .value(c.getValue())
                        .requestInfo(requestInfo)
                        .build();
            }).forEach(cookieRepository::save);
        }

        // create new parallel thread
        Thread getGeoLocation = new Thread(() -> {

            // save geolocation
            GeoLocationDto geoLocationDto = locationRepository.findGeoByIP(IpResolver.getRequestIp(servletRequest));
            if (geoLocationDto != null) {
                requestInfo.setCountry(geoLocationDto.getCountry());
                requestInfo.setCity(geoLocationDto.getCity());
                requestInfo.setLongitude(geoLocationDto.getLongitude());
                requestInfo.setLatitude(geoLocationDto.getLatitude());
                requestRepository.save(requestInfo);
            }

            // request and save weather conditions
            YaMessageDto yaMessageDto = requester.requestProvider(geoLocationDto);
            if (yaMessageDto != null) {
                requestInfo.setTemperature(yaMessageDto.getFact().getTemp());
                requestInfo.setWeatherCondition(yaMessageDto.getFact().getCondition());
                requestRepository.save(requestInfo);
            }
        });

        // start the thread
        getGeoLocation.start();

        // check the expiration date, change the state and delete the owner if expired
        if (Reference.validate(ReferenceValidator::isExpired, reference)) {
            reference.setState(Reference.State.NOT_ACTIVE);
            reference.setOwnerUser(null);
            referenceRepository.save(reference);
        }

        // check state
        if (reference.getState().equals(Reference.State.NOT_ACTIVE)) {
            // if NOT_ACTIVE - return reference's page
            url = "/reference/" + reference.getShortRef();
        } else {
            // else save a successful redirection and return final URL
            requestInfo.setRedirected(true);
            requestRepository.save(requestInfo);
            url = reference.getRedirectRef();
        }
        return url;
    }
}
