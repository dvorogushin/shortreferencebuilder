package com.example.shortreferencebuilder.services.impl;

import com.example.shortreferencebuilder.repositories.GeoLocationRepository;
import com.example.shortreferencebuilder.requester.weatherRequester.YaMessageRequester;
import com.example.shortreferencebuilder.services.WeatherFeatureService;
import com.example.shortreferencebuilder.transfer.geolocationDto.GeoLocationDto;
import com.example.shortreferencebuilder.transfer.yaWeatherDto.YaMessageDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Date: 03.02.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * Service implementation class to deal with Weather Feature on a main page.
 * <p>Defines:
 * <ul>
 *     <li>geographic coordinated by IP</li>
 *     <li>weather temperature by coordinates</li>
 * </ul>
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Service
@RequiredArgsConstructor
public class WeatherFeatureServiceImpl implements WeatherFeatureService {

    /**
     * Repository object to define coordinates.
     */
    @Autowired
    private final GeoLocationRepository locationRepository;

    /**
     * Requester object to define temperature.
     */
    private final YaMessageRequester requester;

    /**
     * Creates requests:
     * <ul>
     *     <li>to database for coordinates</li>
     *     <li>to Yandex.Weather.API for temperature</li>
     * </ul>
     *
     * @param ip IP address as string
     * @return temperature as string
     */
    @Override
    public String getWeatherTemp(String ip) {

        GeoLocationDto geoLocationDto = getGeoLocation(ip);
        if (geoLocationDto != null) {
            YaMessageDto yaMessageDto = requester.requestProvider(geoLocationDto);
            if (yaMessageDto != null) {
                return String.valueOf(yaMessageDto.getFact().getTemp());
            }
        }
        return "";
    }

    /**
     * Creates request to a database to define coordinates by IP.
     *
     * @param ip IP address as string
     * @return {@link GeoLocationDto} object.
     */
    @Override
    public GeoLocationDto getGeoLocation(String ip) {
        return locationRepository.findGeoByIP(ip);
    }

}
