package com.example.shortreferencebuilder.services.impl;

import com.example.shortreferencebuilder.models.User;
import com.example.shortreferencebuilder.repositories.UserRepository;
import com.example.shortreferencebuilder.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Date: 24.01.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * User's service implementation class.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    /**
     * {@link User} repository object.
     */
    private final UserRepository userRepository;

    /**
     * Finds {@link User user} by ID.
     *
     * @param id User's ID
     * @return {@link User} object.
     */
    @Override
    public User getUser(Long id) {
        User user = userRepository.getById(id);
        return user;
    }
}
