package com.example.shortreferencebuilder.services.impl;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * Date: 15.04.2022
 * <p>Project: <a href="https://gitlab.com/dvorogushin/shortreferencebuilder">shortreferencebuilder</a></p>
 * <p> Checks if request is from local host or private net.
 * Actual when app is running on local machine.
 *
 * @author Dmitry Vorogushin (<a href="mailto:dvorogushin@gmail.com">mail</a>, <a href="https://gitlab.com/dvorogushin">GitLab</a>)
 */
public class IpResolver {

    /**
     * Checks if request is from local host or private net.
     * If true then defines and returns public (router) ip.
     *
     * @param request
     * @return ip as string.
     */
    public static String getRequestIp(HttpServletRequest request) {

        String ip = request.getRemoteAddr();

        // if IP is related to localhost or private network
        if (ip == null || ip.startsWith("127") || ip.startsWith("192")) {

            // send a request to ipify.org to define a public IP of a net router
            try {
                HttpRequest req = HttpRequest.newBuilder()
                        .uri(new URI("https://api.ipify.org"))
                        .build();
                try {
                    HttpResponse<String> res = HttpClient.newHttpClient()
                            .send(req, HttpResponse.BodyHandlers.ofString());
                    ip = res.body();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return ip;
    }
}